<?php

namespace exoo\autosize;

use yii\web\AssetBundle;

/**
 * AutosizeAsset bundle.
 * @see http://www.jacklmoore.com/autosize/
 */
class AutosizeAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@npm/autosize/dist';
    /**
     * @inheritdoc
     */
    public $js = [
        'autosize.min.js',
    ];
    /**
     * @inheritdoc
     */
    public $depends = [
        'exoo\exookit\ExookitAsset',
    ];
}
